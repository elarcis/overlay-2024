import './assets/style.pcss';

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import { useStreamElements } from './stores/streamElements';
import { useLayoutStore } from './stores/layout';

const app = createApp(App);

app.use(createPinia());

app.mount('#app');

if (__DEV_FIELDS__) {
  useStreamElements().init(__DEV_FIELDS__, { data: {} });
} else {
  useStreamElements().registerGlobalListeners();
}

useLayoutStore().init();
