import logger from '@/utils/logger';
import {
  WindowEvent,
  WindowEventType,
  type Events,
  type FieldData,
  type Session,
} from '@/utils/streamelements';
import { defineStore } from 'pinia';
import EventEmitter from 'eventemitter3';
import { useWizebot } from './wizebot';

const log = logger('StreamElements');

export const useStreamElements = defineStore('streamElements', {
  state: () => ({
    emitter: new EventEmitter<Events>(),
    session: null as Session | null,
    fields: null as FieldData | null,
    registered: false,
  }),

  actions: {
    registerGlobalListeners() {
      if (this.registered) {
        return;
      }
      window.addEventListener(WindowEvent.EventReceived, function (obj) {
        if (obj.detail?.event == null) {
          return;
        }
        useStreamElements().processEvent(obj.detail.listener, obj.detail.event);
      });

      window.addEventListener(
        WindowEvent.WidgetLoad,
        function ({ detail: { session, fieldData } }) {
          const streamElements = useStreamElements();
          log('received field data', fieldData);
          log('setting session data', session);
          streamElements.init(fieldData, session);
        },
      );
      this.registered = true;
    },
    init(fields: FieldData, session: Session) {
      this.fields = fields;
      this.session = session;
      if (this.session.data[WindowEventType.CheerLatest]) {
        this.processEvent(
          WindowEventType.CheerLatest,
          this.session.data[WindowEventType.CheerLatest],
        );
      }
      if (this.session.data[WindowEventType.SubscriberLatest]) {
        this.processEvent(
          WindowEventType.SubscriberLatest,
          this.session.data[WindowEventType.SubscriberLatest],
        );
      }
      if (this.fields.wizebotKey !== '' && this.fields.wizebotSecret !== '') {
        useWizebot().openConnection(
          this.fields.wizebotKey,
          this.fields.wizebotSecret,
        );
      }
    },
    on<T extends keyof Events>(
      eventType: T,
      cb: EventEmitter.EventListener<Events, T>,
    ): () => void {
      this.emitter.addListener(eventType, cb);
      return () => this.emitter.removeListener(eventType, cb);
    },
    async processEvent<T extends keyof Events>(
      eventType: T,
      event: Events[T][0],
    ) {
      this.emitter.emit(eventType, event as unknown as never);
    },
  },
});
