import { useWizebot } from '@/stores/wizebot';
import { defineStore } from 'pinia';
import { BarCommand, ChatCommand, WizebotCommand } from '../utils/wizebot';
import type { BarState, LogoState } from '@/types/layout';

const BAR_STATES = {
  [BarCommand.Hide]: 'hidden',
  [BarCommand.Expand]: 'expanded',
  [BarCommand.Collapse]: 'collapsed',
} as const;

const CHAT_WIDTHS = {
  [ChatCommand.Expand]: 580,
  [ChatCommand.Compact]: 550,
  [ChatCommand.Hide]: 0,
} as const;

export const useLayoutStore = defineStore('layout', {
  state: () => ({
    barState: BAR_STATES[BarCommand.Collapse] as BarState,
    logoState: 'full' as LogoState,
    logoWidthPx: 580,
    chatWidthPx: CHAT_WIDTHS[ChatCommand.Expand] as number,
  }),

  getters: {
    logoBackgroundVisible: (state) => state.barState !== 'expanded',
  },

  actions: {
    init() {
      const wizebot = useWizebot();
      wizebot.on(WizebotCommand.Bar, (command) => {
        this.logoState = command === BarCommand.Hide ? 'collapsed' : 'full';
        this.barState = BAR_STATES[command];
      });
      wizebot.on(WizebotCommand.Chat, (command) => {
        this.chatWidthPx = CHAT_WIDTHS[command];
      });
    },
  },
});
