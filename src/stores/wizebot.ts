import logger from '@/utils/logger';
import { WizebotCommand, type Commands } from '@/utils/wizebot';
import EventEmitter from 'eventemitter3';
import { defineStore } from 'pinia';
import type { ManagerOptions, SocketOptions, Socket } from 'socket.io-client';

// Imported via a <script> tag
declare const io: (
  uri: string | Partial<ManagerOptions & SocketOptions>,
  opts?: Partial<ManagerOptions & SocketOptions>,
) => Socket;

const log = logger('Wizebot');

export const useWizebot = defineStore('wizebot', {
  state: () => ({
    url: 'https://sockets.wizebot.tv:21001',
    emitter: new EventEmitter<Commands>(),
  }),

  actions: {
    on<T extends keyof Commands>(
      eventType: T,
      cb: EventEmitter.EventListener<Commands, T>,
    ): () => void {
      this.emitter.addListener(eventType, cb);
      return () => this.emitter.removeListener(eventType, cb);
    },
    openConnection(key: string, secret: string) {
      log('Opening web socket for URL: ', this.url);

      const socket = io(this.url, {
        autoConnect: false,
        reconnection: true,
        reconnectionDelay: 1000,
      }).connect();

      socket.on('is_connected', () => {
        log('connected! Sending auth event to Wizebot');
        socket.emit('auth', key, secret);
      });

      socket.on('connect_error', (err) =>
        console.error('Wizebot connection error:', err),
      );

      socket.on('dispatch', (event: any) => {
        log('event received:', event);
        if (event.event_name === 'api-call') {
          let eventType: unknown, eventData: string | object;

          if (typeof event.event_datas === 'object') {
            eventType = event.event_datas.kind;
            eventData = event.event_datas.data;
          } else {
            [eventType, eventData] = event.event_datas.split('.');
          }

          if (eventType == null) {
            throw new Error('Event type not specified');
          }
          if (!isWizebotCommand(eventType)) {
            throw new Error(`Event type not recognised: ${eventType}`);
          }

          this.emitter.emit(eventType, eventData as unknown as never);
        }
      });
    },
  },
});

function isWizebotCommand(eventType: unknown): eventType is WizebotCommand {
  return (
    typeof eventType === 'string' &&
    (Object.values(WizebotCommand) as string[]).includes(eventType)
  );
}
