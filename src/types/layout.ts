export type BarState = 'collapsed' | 'expanded' | 'hidden';
export type LogoState = 'full' | 'collapsed';
