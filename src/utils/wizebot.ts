export enum WizebotCommand {
  Bar = 'bar',
  Chat = 'chat',
  Fauteuil = 'fauteuil',
  Layout = 'layout',
  Timer = 'timer',
  Quote = 'quote',
}

export enum BarCommand {
  Expand = 'expand',
  Collapse = 'collapse',
  Hide = 'hide',
}

export enum ChatCommand {
  Expand = 'expand',
  Compact = 'compact',
  Hide = 'hide',
}

export enum LayoutCommand {
  Expand = 'expand',
  Collapse = 'collapse',
}

export interface Commands {
  [WizebotCommand.Bar]: [BarCommand];
  [WizebotCommand.Chat]: [ChatCommand];
  [WizebotCommand.Fauteuil]: [FauteuilCommand];
  [WizebotCommand.Layout]: [LayoutCommand];
  [WizebotCommand.Quote]: [QuoteCommand];
  [WizebotCommand.Timer]: [TimerCommand];
}

export interface FauteuilCommand {
  newUser: string;
}

export interface QuoteCommand {
  id: number;
  text: string;
  category: string;
  sender: string;
  created_at: string;
  love: number;
}

export interface TimerCommand {
  target: string;
  absolute: boolean;
}
