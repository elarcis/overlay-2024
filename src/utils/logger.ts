export default function logger(name: string) {
  return function log(...data: unknown[]) {
    console.log(`[${name.toLocaleUpperCase()}]`, ...data);
  };
}
