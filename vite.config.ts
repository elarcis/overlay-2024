import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';

import { config } from 'dotenv';

config();

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [vue()],
  define: {
    __DEV_FIELDS__:
      mode === 'production'
        ? undefined
        : {
            wizebotKey: process.env.DEV_WIZEBOT_KEY,
            wizebotSecret: process.env.DEV_WIZEBOT_SECRET,
            widgetName: 'Wizebot Bridge',
            widgetAuthor: 'Elarcis',
            backgroundPattern:
              'https://cdn.streamelements.com/uploads/495e31fc-8b7f-43ca-8021-87eef586abe9.png',
          },
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
}));
