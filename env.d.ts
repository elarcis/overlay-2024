/// <reference types="vite/client" />

import type {
  EventObject,
  Events,
  FieldData,
  WidgetLoadEventObject,
  WindowEvent,
} from '@/utils/streamelements';
import '@types/jquery';

declare module '*.pcss';
declare module 'nearest-color';

declare global {
  const SE_API: StreamElements.Api;

  const __DEV_FIELDS__: FieldData | undefined;

  interface WindowEventMap {
    [WindowEvent.EventReceived]: EventObject<keyof Events>;
    [WindowEvent.WidgetLoad]: WidgetLoadEventObject;
  }
}

declare module StreamElements {
  interface Api {
    store: Store;
    counters: Counters;
    sanitize(value: { message: string }): Promise<SanitizedMessage>;
  }

  interface Store {
    set<T>(key: string, value: T);
    get<T>(key: string): Promise<T>;
  }

  interface Counters {
    get(key: string): Promise<Counter>;
  }

  interface Counter {
    counter: string;
    value: number;
  }

  interface SanitizedMessage {
    result: { message: string };
    skip: boolean;
  }

  interface KVStoreEvent<T> {
    data: { key: string; value: T };
  }

  interface CheerEvent {
    amount: number;
    name: string;
  }

  interface SubEvent {
    amount: number;
    bulkGifted: boolean;
    name: string;
    tier: number;
  }
}
