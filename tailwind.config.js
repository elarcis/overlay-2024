import { black, white, gray, transparent, current } from 'tailwindcss/colors';

/** @type {import('tailwindcss').Config} */
export default {
  content: ['index.html', 'src/**/*.{ts,vue}'],
  theme: {
    colors: {
      transparent,
      current,
      black,
      white,
      gray,
      green: {
        50: '#e7f6f1',
        100: '#cfede4',
        200: '#a0dbc9',
        300: '#70c8ad',
        400: '#41b692',
        500: '#11a477',
        600: '#0e835f',
        700: '#0a6247',
        800: '#074230',
        900: '#032118',
      },
      purple: {
        50: '#eae9ef',
        100: '#d4d2de',
        200: '#aaa5bd',
        300: '#7f789d',
        400: '#554b7c',
        500: '#2a1e5b',
        600: '#221849',
        700: '#191237',
        800: '#110c24',
        900: '#080612',
      },
      yellow: {
        50: '#fefae8',
        100: '#fef4d1',
        200: '#fceaa3',
        300: '#fbdf76',
        400: '#f9d548',
        500: '#f8ca1a',
        600: '#c6a215',
        700: '#957910',
        800: '#63510a',
        900: '#322805',
      },
    },
    extend: {
      animation: {
        'slide-diagonal': 'slide-diagonal 30s linear infinite',
      },
      keyframes: {
        'slide-diagonal': {
          '0%': { backgroundPosition: '0 0' },
          '100%': { backgroundPosition: '1024px 1008px' },
        },
      },
    },
  },
  plugins: [],
};
